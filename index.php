<?php 
session_start();
require_once 'app/Core/Core.php';
require_once 'app/Controller/HomeController.php';
require_once 'app/Controller/ErroController.php';
require_once 'app/Controller/PesquisaController.php';
require_once 'app/Controller/CadastroClienteController.php';
require_once 'app/Controller/HospedagemController.php';
require_once 'app/Controller/RetiradaController.php';
require_once 'app/Controller/AuxController.php';
require_once 'app/Controller/SobreController.php';
require_once 'app/Controller/EditarClienteController.php';
require_once 'app/Model/Pokemon.php';
require_once 'app/Model/Cliente.php';
require_once 'app/Model/Especie.php';
require_once 'app/Model/Plano.php';
require_once 'lib/Database/Connection.php';
require_once 'vendor/autoload.php';

$template = file_get_contents('app/Template/Template.php');

ob_start();
    $core = new Core;
    $core->start($_GET);
    $saida = ob_get_contents();
ob_end_clean();

$tpl = str_replace('{{area dinamica}}',$saida,$template);

echo $tpl;