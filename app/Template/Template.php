<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>HotelPokemon</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
  </head>
  <body>
    <nav class="navbar is-success">
      <div class="container">
          <div class="navbar-brand">
              <a class="navbar-item" href="http://localhost/hotelpokemon/?pagina=home" >
                      <img src="img/pokeball.png" alt="Logo" width="30px">
                      HotelPokemon
              </a>
              <span class="navbar-burger burger" data-target="navbarMenu">
                      <span></span>
              <span></span>
              <span></span>
              </span>
          </div>
          <div id="navbarMenu" class="navbar-menu">
              <div class="navbar-end">
                    <a href="http://localhost/hotelpokemon/?pagina=home" class="navbar-item is-active">
                          Home
                    </a>
                    <a href="http://localhost/hotelpokemon/?pagina=cadastroCliente" class="navbar-item">
                          Cadastro cliente
                    </a>
                    <a href="http://localhost/hotelpokemon/?pagina=pesquisa" class="navbar-item">
                        Pesquisa
                    </a>    
                    <a href="http://localhost/hotelpokemon/?pagina=sobre" class="navbar-item">
                        Sobre
                    </a>
              </div>
          </div>
      </div>
    </nav>

    <section class="hero">
        <div class="hero-body">
            <div class="columns is-centered">
                <div class="column is-one-quarter">
                    <aside class="menu">
                        <p class="menu-label">
                            HotelPokemon!
                        </p>
                        <ul class="menu-list">
                            <li>
                                <a href="http://localhost/hotelpokemon/?pagina=home">Home </a>
                            </li>
                            <li>
                                <a href="http://localhost/hotelpokemon/?pagina=cadastroCliente">Cadastro Cliente </a>
                            </li>
                            <li>
                                <a href="http://localhost/hotelpokemon/?pagina=pesquisa">Pesquisa </a>
                            </li>
                            <li>
                            <a href="http://localhost/hotelpokemon/?pagina=sobre">Sobre </a>
                            </li>
                        </ul>
                    </aside> 
                </div> 
                <div class="column">
                    {{area dinamica}}
                </div>
            </div>
        </div>
    </section>

    <footer class="footer has-text-centered">
        <strong>
            HotelPokemon criado por Leornado Alecrim e André Luis dos Santos
            <div>Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
        </strong>
    </footer>


  </body>
</html>

<script>
document.addEventListener('DOMContentLoaded', () => {

// Get all "navbar-burger" elements
const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

// Check if there are any navbar burgers
if ($navbarBurgers.length > 0) {

  // Add a click event on each of them
  $navbarBurgers.forEach( el => {
    el.addEventListener('click', () => {

      // Get the target from the "data-target" attribute
      const target = el.dataset.target;
      const $target = document.getElementById(target);

      // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
      el.classList.toggle('is-active');
      $target.classList.toggle('is-active');

    });
  });
}

});

</script>