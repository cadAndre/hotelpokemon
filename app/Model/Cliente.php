<?php

    class Cliente{
        public static function clienteById($id){
            $conexao = Connection::getConn();

            $sql = "SELECT * FROM Cliente where idCliente = '$id'";

            $sql = $conexao->prepare($sql);
            $sql->execute();

            $resultado = array();

            while ($row = $sql->fetchObject('Cliente')){
                $resultado[] = $row;
                $_SESSION['id'] = $resultado[0]->idCliente;
            }

            return $resultado;
        }
        public static function contar($id){
            $conexao = Connection::getConn();

            $sql = "select count(idPokemon) from Pokemon where idDono = '$id';";

            $sql = $conexao->prepare($sql);
            $sql->execute();

            $resultado = array();

            $row = $sql->fetch();

            return $row[0];
        }

        public static function selecionaCliente($cpf){
            $conexao = Connection::getConn();

            $sql = "SELECT * FROM Cliente where cpf = '$cpf'";

            $sql = $conexao->prepare($sql);
            $sql->execute();

            $resultado = array();

            while ($row = $sql->fetchObject('Cliente')){
                $resultado[] = $row;
                $_SESSION['id'] = $resultado[0]->idCliente;
            }

            return $resultado;
        }
        public static function cadastrar($nome, $cpf, $tel,$idPlano){
            $conexao = Connection::getConn();

            $sql = "insert into Cliente(nomeCompleto, cpf, telefone, idPlano) value ('$nome', '$cpf', '$tel', '$idPlano')";
            $sql = $conexao->prepare($sql);
            $sql->execute();  
            
            if($sql->rowCount()){
                $loader = new \Twig\Loader\FilesystemLoader('app/View');
                $twig = new \Twig\Environment($loader);
                $template = $twig->load('SuccessInsertCliente.html');

                $conteudo = $template->render();
                return $conteudo;
            }
            else{
                $loader = new \Twig\Loader\FilesystemLoader('app/View');
                $twig = new \Twig\Environment($loader);
                $template = $twig->load('FailInsertCliente.html');

                $conteudo = $template->render();
                return $conteudo;
            }
        }

        public static function Editar($cpf, $tel,$idPlano){
            $conexao = Connection::getconn();

            $sql = "UPDATE `Cliente` SET `telefone` = '$tel', `idPlano` = '$idPlano' WHERE cpf = '$cpf' ";

            $sql = $conexao->prepare($sql);

            $sql->execute();

            if($sql->rowCount()){
                $loader = new \Twig\Loader\FilesystemLoader('app/View');
                $twig = new \Twig\Environment($loader);
                $template = $twig->load('SuccessEdit.html');

                $conteudo = $template->render();
                return $conteudo;
            }
            else{
                $loader = new \Twig\Loader\FilesystemLoader('app/View');
                $twig = new \Twig\Environment($loader);
                $template = $twig->load('FailEdit.html');

                $conteudo = $template->render();
                return $conteudo;
            }
            throw new Exception("Falha ao editar cliente.");
        }
    }