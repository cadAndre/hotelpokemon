<?php 

    class Especie{

        public static function selecionaTodos(){
            $conexao = Connection::getConn();

            $sql = "SELECT * FROM Especie";

            $sql = $conexao->prepare($sql);
            $sql->execute();

            $resultado = array();

            while ($row = $sql->fetchObject('Especie')){
                $resultado[] = $row;
            }
            
            return $resultado;
        }
    }