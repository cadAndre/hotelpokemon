<?php 
    class Pokemon{
        public static function selecionaTodos($x){
            $conexao = Connection::getConn();

            $sql = "SELECT * FROM vwPokemon where IdCliente = $x";
            $sql = $conexao->prepare($sql);
            $sql->execute();

            $resultado = array();

            while ($row = $sql->fetchObject('Pokemon')){
                $resultado[] = $row;
            }

            return $resultado;
        }
        public static function pesquisar(){
            $conexao = Connection::getConn();

            $sql = "SELECT * FROM Pokemon";
            $sql = $conexao->prepare($sql);
            $sql->execute();

            $resultado = array();

            while ($row = $sql->fetchObject('Pokemon')){
                $resultado[] = $row;
            }
            if(!$resultado){
                throw new Exception("Nenhum pokemon encontrado");
            }
            return $resultado[0];
        }
        public static function pesquisarPokemon($idPokemon){
            $conexao = Connection::getConn();

            $sql = "SELECT * FROM Pokemon where id = $idPokemon";
            $sql = $conexao->prepare($sql);
            $sql->execute();

            $resultado = array();

            while ($row = $sql->fetchObject('Pokemon')){
                $resultado[] = $row;
            }
            if(!$resultado){
                throw new Exception("Nenhum pokemon encontrado");
            }
            return $resultado[0];
        }
        #nao funfa
        public static function cadastrar($nome, $nivel, $sexo, $idEspecie){
            $conexao = Connection::getConn();
            if(self::contar($_SESSION['id'])){

                date_default_timezone_set('America/Bahia');
                $data = date('Y/m/d H:i:s');
                $experiencia = random_int( 0, 350);
    
                $sql = "insert into Pokemon(nome, dataEntrada, nivel, sexo, experiencia, idDono, idEspecie, taxa) 
                value ('$nome', '$data', $nivel, '$sexo', $experiencia, :id, $idEspecie, 50.0)";
                $sql = $conexao->prepare($sql);
    
                $sql->bindValue(':id', $_SESSION['id']);
    
                $sql->execute();
                
                if($sql->rowCount()){
                    $loader = new \Twig\Loader\FilesystemLoader('app/View');
                    $twig = new \Twig\Environment($loader);
                    $template = $twig->load('SuccessInsertPokemon.html');
    
                    $conteudo = $template->render();
                    return $conteudo;
                }
                else{
                    $loader = new \Twig\Loader\FilesystemLoader('app/View');
                    $twig = new \Twig\Environment($loader);
                    $template = $twig->load('FailInsertPokemon.html');
    
                    $conteudo = $template->render();
                    return $conteudo;
                }
            }
            else{
                echo '<h1 class="title"> limite de Pokemons no seu plano alcançado</h1>'; 
                echo '<p class="subtitle"> Tente assinar outro Plano</p>';
                echo '<img src="img/Fails.png" width="300">';
                return true;
            }
            
        }
        public static function deletar($id){
            $conexao = Connection::getconn();

            $sql = "delete from Pokemon where idPokemon = $id";
            $sql = $conexao->prepare($sql);

            $sql->execute();

            $resultado = '<h1 class="title"> Pokemon retirado</h1>' . '<p class="subtitle"> Curta por ai com seu parceiro</p>' .'<img src="img/Success.png" width="300">';
            return $resultado;
        }

        public static function editar($id, $nome, $sexo){
            $conexao = Connection::getconn();

            $sql = "update Pokemon set nome = '$nome' and sexo = '$sexo' where id = $id";

            $sql = $conexao->prepare($sql);

            $sql->execute();

            if($sql->rowCount()){
                return true;
            }

            throw new Exception("Falha ao editar pokemon.");
        }

        public static function levelUp($lista){
            
            date_default_timezone_set('America/Bahia');
            $data = date('Y-m-d H:i:s');
            $datetime1 = new DateTime($data);
            
            foreach($lista as $pokemon){
                $datetime2 = new DateTime($pokemon->dataEntrada);
    
                $data1 = $datetime1->format('Y-m-d H:i:s');
                $data2 =$datetime2->format('Y-m-d H:i:s');
                //var_dump($datetime2);
    
                $diff = $datetime1->diff($datetime2);
                $minutos = $diff->days * 24 * 60;
                $minutos += $diff->h * 60;
                $minutos += $diff->i;
    
                self::converter($pokemon, $minutos);
                
            }
            
        }
        private function converter($pokemon, $minutos){
            $pokemon->xpTotal = $minutos * $pokemon->expPorTempo;
            $pokemon->experiencia += $pokemon->xpTotal;
            while($pokemon->experiencia >= $pokemon->expPorNivel){
                $pokemon->experiencia -= $pokemon->expPorNivel;
                $pokemon->nivel ++;
                
            }

            $pokemon->taxa += $pokemon->custoPorTempo * (int) ($minutos / 60);
            return $pokemon;
        }
        public static function contar($id){
            $conexao = Connection::getConn();
            
            $sql = "select count(Pokemon.idPokemon) as quantidade, Plano.limiteDePokemons from Pokemon join Cliente on Pokemon.idDono = Cliente.idCliente join Plano on Cliente.idPlano = Plano.idPlano where Cliente.idCliente = $id";
        
            $sql = $conexao->prepare($sql);
            $sql->execute();
        
            $resultado = array();
        
            $row = $sql->fetchObject();
    
            if($row->quantidade < $row->limiteDePokemons){
                return true;
            }
            return false;
        }
    }
    