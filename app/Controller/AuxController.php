<?php

    class AuxController{

        public function index(){

            try{
                $x = $_POST['cpf'];
                $colec = Cliente::selecionaCliente($x);

                $loader = new \Twig\Loader\FilesystemLoader('app/View');
                $twig = new \Twig\Environment($loader);
                $template = $twig->load('Cliente.html');

                $parametros = array();
                $parametros['Clientes'] = $colec;

                $conteudo = $template->render($parametros);
                echo $conteudo;

            } 
            catch(Exeception $e){
                echo $e->getMessage();
            }
        }
    }