<?php
    class PesquisaController{

        public function index(){

            try{

                $loader = new \Twig\Loader\FilesystemLoader('app/View');
                $twig = new \Twig\Environment($loader);
                $template = $twig->load('Pesquisa.html');

                $conteudo = $template->render();
                echo $conteudo;

            } 
            catch(Exeception $e){
                echo $e->getMessage();
            }
        }
        public function pesquisar(){
            try{
                $x = $_POST['cpf'];
                $colec = Cliente::selecionaCliente($x);

                $loader = new \Twig\Loader\FilesystemLoader('app/View');
                $twig = new \Twig\Environment($loader);
                $template = $twig->load('Pesquisa.html');

                $conteudo = $template->render();
                echo $conteudo;

                $loader = new \Twig\Loader\FilesystemLoader('app/View');
                $twig = new \Twig\Environment($loader);
                $template = $twig->load('Cliente.html');

                $parametros = array();
                $parametros['Clientes'] = $colec;

                if($colec){
                $plano['planos'] = Plano::selecionaById($colec[0]->idPlano);

                $hospedados['atual'] = Cliente::contar($colec[0]->idCliente);
                
                $parametros = $parametros + $plano + $hospedados;
                }
                
                $conteudo = $template->render($parametros);
                echo $conteudo;

            }
            catch(Exception $e){
                echo $e->getMessage();
            }
        }
    }