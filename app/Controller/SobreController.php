<?php

    class SobreController{

        public function index(){
            $colecPlanos = Plano::selecionaTodos();

            $loader = new \Twig\Loader\FilesystemLoader('app/View');
            $twig = new \Twig\Environment($loader);
            $template = $twig->load('Sobre.html');

            $parametros = array();
            $parametros['planos'] = $colecPlanos;

            $conteudo = $template->render($parametros);
            echo $conteudo;
        }
    }