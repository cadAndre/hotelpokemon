<?php
    class RetiradaController{

        public function index(){
            
            try{
                $x =($_GET["id"]);
                
                $colecPokemons = Pokemon::selecionaTodos($x);
                
                $loader = new \Twig\Loader\FilesystemLoader('app/View');
                $twig = new \Twig\Environment($loader);
                $template = $twig->load('Retirada.html');
                
                $parametros = array();
                $parametros['pokemons'] = $colecPokemons;

                $atualizado = Pokemon::levelUp($colecPokemons);
                
                
                $conteudo = $template->render($parametros);
                echo $conteudo;

            } 
            catch(Exception $e){
                echo $e->getMessage();
            }
        }
        public function deletar(){
            $id = $_GET["id"];

            try{
               echo Pokemon::deletar($id);
            }
            catch(Exception $e){
                echo '<h1 class="title"> Ops ...</h1>' . '<p class="subtitle"> Tivemos algum erro no sistema</p>' .'<img src="img/Fails.png" width="300">';
                $e->getMessage();
            }
        }
    }