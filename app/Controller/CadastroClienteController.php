<?php
    class CadastroClienteController{

        public function index(){

            try{
                $colec = Plano::selecionaTodos();

                $loader = new \Twig\Loader\FilesystemLoader('app/View');
                $twig = new \Twig\Environment($loader);
                $template = $twig->load('CadastroCliente.html');

                $parametros = array();
                $parametros['planos'] = $colec;

                $conteudo = $template->render($parametros);
                echo $conteudo;
            } 
            catch(Exeception $e){
                echo $e->getMessage();
            }
        }
        public function Cadastrar(){

            try{
                $nome = $_POST["nome"];
                $cpf = $_POST["cpf"];
                $telefone = $_POST["telefone"];
                $plano = $_POST["plano"];
                echo Cliente::Cadastrar($nome, $cpf, $telefone, $plano);
            }
            catch(Exception $e){
                echo $e;
            }

        }
    }