<?php
    class EditarClienteController{

        public function index(){

            try{
                $colec = Plano::selecionaTodos();

                $x = $_GET['id'];

                $ex = Cliente::contar($x);

                if(Cliente::contar($x) != 0){

                    $loader = new \Twig\Loader\FilesystemLoader('app/View');
                    $twig = new \Twig\Environment($loader);
                    $template = $twig->load('NPodeEditar.html');
    
                    $conteudo = $template->render();
                    echo $conteudo;
                    return true;
                }

                $cliente = Cliente::clienteById($x);

                $loader = new \Twig\Loader\FilesystemLoader('app/View');
                $twig = new \Twig\Environment($loader);
                $template = $twig->load('EditarCliente.html');

                $parametros2 =array();
                $parametros2['clientes'] = $cliente; 


                $parametros = array();
                $parametros['planos'] = $colec;  
                
                $parametros2 = $parametros2 + $parametros;

                $conteudo = $template->render($parametros2);
                echo $conteudo;
            } 
            catch(Exeception $e){
                echo $e->getMessage();
            }
        }

        public function Editar(){

            try{
            
                $cpf = $_POST["cpf"];
                $telefone = $_POST["telefone"];
                $plano = $_POST["plano"];
                echo Cliente::Editar( $cpf, $telefone, $plano);
            }
            catch(Exception $e){
                echo $e;
            }

        }
    }