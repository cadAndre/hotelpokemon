<?php
    class HospedagemController{

        public function index(){

            try{
                $colecEspecie = Especie::selecionaTodos();

                $loader = new \Twig\Loader\FilesystemLoader('app/View');
                $twig = new \Twig\Environment($loader);
                $template = $twig->load('Hospedagem.html');

                $parametros = array();
                $parametros['especies'] = $colecEspecie;

                $conteudo = $template->render($parametros);
                echo $conteudo;

            } 
            catch(Exception $e){
                echo $e->getMessage();
            }
        }
        public function CadastrarPokemon(){
            #nao funfa
            try{
                $nome = $_POST["nome"];
                $nivel = $_POST["lv"];
                $idEspecie = $_POST["especie"];
                $sexo = $_POST["sexo"];
                echo Pokemon::Cadastrar($nome, $nivel, $sexo, $idEspecie);

            }
            catch(Exception $e){
                echo $e->getMessage();
            }
        }
    }