CREATE DATABASE IF NOT EXISTS `pokeHotel`;
USE `pokeHotel`;

#drop database pokeHotel;

CREATE TABLE IF NOT EXISTS `Cliente` (
  `idCliente` INT NOT NULL PRIMARY KEY auto_increment,
  `nomeCompleto` VARCHAR(100) NOT NULL,
  `cpf` VARCHAR(50) unique NOT NULL,
  `telefone` VARCHAR(45) NULL,
  idPlano int not null
) ENGINE = InnoDB;

select * from Cliente;

CREATE TABLE IF NOT EXISTS `Especie` (
  `idEspecie` INT NOT NULL PRIMARY KEY,
  `nome` VARCHAR(50) NOT NULL unique,
  `expPorNivel` INT NOT NULL,
  `proxEvolucao` INT NULL,
  `primeiraEvolucao` INT NULL,
  lvup int null
) ENGINE = InnoDB;
insert into Especie value
(001, "Bulbasour", 500, null, null, 16),
(002, "Ivysaur", 500, null, 001, 32),
(003, "Venusaur", 500, null, 002, null),
(004, "Charmander", 600, null, null, 16),
(005, "Charmeleon", 600, null, 004, 36),
(006, "Charizard", 600, null, 005, null),
(007, "Squirtle", 400, null, null, 16),
(008, "Wartutle", 400, null, 007, 36),
(009, "Blastoise", 400, null, 008, null),
(010, "Riolu", 300, null, 008, null),
(011, "Lucario", 300, null, 008, null);

select * from Especie;

update Especie set proxEvolucao = 002 where idEspecie = 001;
update Especie set proxEvolucao = 003 where idEspecie = 002;
update Especie set proxEvolucao = 005 where idEspecie = 004;
update Especie set proxEvolucao = 006 where idEspecie = 005;
update Especie set proxEvolucao = 008 where idEspecie = 007;
update Especie set proxEvolucao = 009 where idEspecie = 008;
update Especie set proxEvolucao = 011 where idEspecie = 010;

CREATE TABLE IF NOT EXISTS `Pokemon` (
  `idPokemon` INT PRIMARY KEY auto_increment,
  `nome` VARCHAR(100) NULL,
  `dataEntrada` datetime NULL,
  `nivel` INT NOT NULL,
  `sexo` set('m', 'f') NULL,
  `experiencia` INT,
  `idDono` INT NOT NULL,
  `idEspecie` INT NOT NULL,
  taxa double not null,
  CONSTRAINT `idCliente`
    FOREIGN KEY (`idDono`)
    REFERENCES `Cliente` (`idCliente`),
  CONSTRAINT `idEspecie`
    FOREIGN KEY (`idEspecie`)
    REFERENCES `Especie` (`idEspecie`)
) ENGINE = InnoDB;
alter table Pokemon add column xpTotal int default 0;
#drop table Pokemon;
#insert into Pokemon(nome, dataEntrada, nivel, sexo, experiencia, idDono, idEspecie) 
#            value ("poke", '1991/11/11', 1, "m", 0, 1, 1);
select * from Pokemon;

CREATE TABLE IF NOT EXISTS `Plano` (
  `idPlano` INT NOT NULL PRIMARY KEY,
  `nome` VARCHAR(100) NOT NULL,
  `custoPorTempo` DOUBLE NOT NULL,
  `expPorTempo` INT NOT NULL,
  `limiteDePokemons` INT NULL,
  descricao TEXT NULL
) ENGINE = InnoDB;

alter table Cliente add constraint foreign key(idPlano) references Plano(idPlano);

INSERT INTO `Plano` (`idPlano`, `nome`, `custoPorTempo`, `expPorTempo`, `limiteDePokemons`, descricao) VALUES ('1', 'Plano Basico', '10', '10', '10', 
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec feugiat ante id nisi gravida, id finibus arcu sollicitudin.
 Suspendisse rutrum dolor dolor, eget bibendum leo pretium non. Ut consequat purus est, in mollis quam ultrices id.
 Vivamus sem orci, vehicula a leo ut, placerat sagittis tortor. Aliquam erat volutpat. Suspendisse.');
INSERT INTO `Plano` (`idPlano`, `nome`, `custoPorTempo`, `expPorTempo`, `limiteDePokemons`, descricao) VALUES ('2', 'Plano Bomzinho', '15', '15', '15', 
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec feugiat ante id nisi gravida, id finibus arcu sollicitudin.
 Suspendisse rutrum dolor dolor, eget bibendum leo pretium non. Ut consequat purus est, in mollis quam ultrices id.
 Vivamus sem orci, vehicula a leo ut, placerat sagittis tortor. Aliquam erat volutpat. Suspendisse.');
 INSERT INTO `Plano` (`idPlano`, `nome`, `custoPorTempo`, `expPorTempo`, `limiteDePokemons`, descricao) VALUES ('3', 'Plano Massa', '20', '20', '20', 
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec feugiat ante id nisi gravida, id finibus arcu sollicitudin.
 Suspendisse rutrum dolor dolor, eget bibendum leo pretium non. Ut consequat purus est, in mollis quam ultrices id.
 Vivamus sem orci, vehicula a leo ut, placerat sagittis tortor. Aliquam erat volutpat. Suspendisse.');

create view vwPokemon as select p.idPokemon, p.nome, p.nivel, p.sexo, p.experiencia, p.dataEntrada, p.taxa, p.xpTotal, e.nome as 'especie', e.lvup, e.idEspecie, e.proxEvolucao, e.expPorNivel, c.idCliente, c.nomeCompleto, c.idPlano as 'dono', a.nome as 'plano', a.idPlano, a.custoPorTempo, a.expPorTempo 
from Pokemon as p join Especie as e join Cliente as c join Plano as a
on (p.idEspecie = e.idEspecie and p.idDono = c.idCliente and a.idPlano = c.idPlano);

#drop view vwPokemon;
select * from Pokemon;
select * from Cliente;
select * from Especie;


select * from vwPokemon;