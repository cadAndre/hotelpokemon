<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hello Bulma!</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
  </head>
  <body>
    <nav class="navbar is-dark">
      <div class="container">
          <div class="navbar-brand">
              <a class="navbar-item" href="../">
                      <img src="img/pokeball.png" alt="Logo" width="30px">
                      HotelPokemon
              </a>
              <span class="navbar-burger burger" data-target="navbarMenu">
                      <span></span>
              <span></span>
              <span></span>
              </span>
          </div>
          <div id="navbarMenu" class="navbar-menu">
              <div class="navbar-end">
                  <a class="navbar-item is-active">
                          Home
                      </a>
                  <a class="navbar-item">
                          Cadastro cliente
                      </a>
                  <a class="navbar-item">
                          Pokemon
                      </a>
                  <a class="navbar-item">
                          Sobre
                      </a>
              </div>
          </div>
      </div>
    </nav>

    <section>

    </section>

    <section class="hero is-dark">
        <div class="hero-body">
            <div class="columns is-centered">
                <div class="column is-one-quarter">
                    <aside class="menu">
                        <p class="menu-label">
                            HotelPokemon!
                        </p>
                        <ul class="menu-list">
                            <li>
                                <a href="#">Home </a>
                            </li>
                            <li>
                                <a href="#">Cadastro Cliente </a>
                            </li>
                            <li>
                                <a href="#">Pokemon </a>
                            </li>
                            <li>
                                <a href="#">Sobre </a>
                            </li>
                        </ul>
                    </aside> 
                </div> 
                <div class="column">
                    {{area dinamica}}
                </div>
            </div>
        </div>
    </section>

    <footer class="footer has-text-centered">
        <strong>
            Pokeparada criado por Leornado Alegrim e André Luis dos Santos
        </strong>
    </footer>


  </body>
</html>

<script>
document.addEventListener('DOMContentLoaded', () => {

// Get all "navbar-burger" elements
const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

// Check if there are any navbar burgers
if ($navbarBurgers.length > 0) {

  // Add a click event on each of them
  $navbarBurgers.forEach( el => {
    el.addEventListener('click', () => {

      // Get the target from the "data-target" attribute
      const target = el.dataset.target;
      const $target = document.getElementById(target);

      // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
      el.classList.toggle('is-active');
      $target.classList.toggle('is-active');

    });
  });
}

});

</script>